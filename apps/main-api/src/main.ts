import { env } from "@app/common/env";
import { ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import cookieParser from "cookie-parser";
import { WINSTON_MODULE_NEST_PROVIDER } from "nest-winston";
import * as winston from "winston";
import cors from "cors";

import { AppModule } from "./app.module";
import { useSwagger } from "./loaders/swaggerLoader";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
    app.enableCors();
    app.use(cookieParser());
    // const whitelist = ['http://localhost:3000', 'http://example2.com'];
    // const corsOptions = {
    //   credentials: true, // This is important.
    //   origin: (origin, callback) => {
    //     if(whitelist.includes(origin))
    //       return callback(null, true)
    
    //       callback(new Error('Not allowed by CORS'));
    //   }
    // }
    
    // app.use(cors(corsOptions));
    // app.use(function (req, res, next) {
    //     res.header("Access-Control-Allow-Origin", "*");
    //     res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    //     res.header(
    //         "Access-Control-Allow-Headers",
    //         "Content-Type, Authorization, Content-Length, X-Requested-With, Accept",
    //     );
    // });
    app.setGlobalPrefix(env.app.routePrefix);
    app.useGlobalPipes(new ValidationPipe({ transform: true, forbidUnknownValues: true }));
    useSwagger(app);
    await app.listen(+env.app.port || 4000);
}

bootstrap().catch((err) => {
    winston.error(err);
    console.error(err);
});
