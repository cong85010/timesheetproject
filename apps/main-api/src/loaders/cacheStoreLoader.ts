import { CacheModule, DynamicModule, Global, Module } from "@nestjs/common";
import { create } from "cache-manager-redis-store";

import { env } from "@app/common/env";

@Global()
@Module({})
export class CacheStoreLoader {
    static register(): DynamicModule {
        if (!env.redis.caching) {
            return { module: CacheStoreLoader, imports: [CacheModule.register()] };
        }
        return {
            module: CacheStoreLoader,
            imports: [
                CacheModule.register({
                    store: create({
                        url: env.redis.url,
                    }),
                    ttl: env.redis.cache_time,
                }),
            ],
            exports: [CacheModule],
        };
    }
}
