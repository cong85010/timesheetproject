import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";

import { IdentityV2Strategy } from "./identity-v2.strategy";
import { IdentityV2Guard } from "./identity-v2.guard";

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: "identity-v2",
            session: false,
        }),
    ],
    providers: [IdentityV2Strategy, IdentityV2Guard],
    exports: [PassportModule, IdentityV2Strategy, IdentityV2Guard],
})
export class AuthenticationModule {}
