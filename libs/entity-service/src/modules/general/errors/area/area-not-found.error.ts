import { HttpException } from "@nestjs/common";

export class AreaNotFoundError extends HttpException {
    constructor() {
        super("Area not found!", 404);
    }
}
