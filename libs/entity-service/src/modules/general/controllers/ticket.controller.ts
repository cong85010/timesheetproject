import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import moment from "moment";
import { CreateTicketDto, UpdateTicketDto } from "../dto/ticket.dto";
import { TicketService } from "../services/ticket.service";
// import { UpdateUserDto } from './dto/update-user.dto';

@Controller("ticket")
export class TicketController {
    constructor(private readonly ticketService: TicketService) {}

    @Post()
    createTicket(@Body() createTicketDto: CreateTicketDto) {
        return this.ticketService.createTicket(createTicketDto);
    }
    @Put()
    updateTicket(@Body() updateTicketDto: UpdateTicketDto) {
        return this.ticketService.updateTicket(updateTicketDto);
    }

    @Delete(":id")
    removeTicket(@Param("id") id: string) {
        return this.ticketService.deleteTicket(id);
    }

    @Get(":id")
    findTicketByUserId(@Param("id") id: string) {
        return this.ticketService.findTicketByUserId(id);
    }

    @Post("/date")
    findTicketByDateId(@Body() userId_Date: { userId: string; date: Date }) {
        const { userId, date } = userId_Date;
        return this.ticketService.findTicketByDateId(userId, date);
    }
}
