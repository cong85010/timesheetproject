import { Controller, Get, Post, Body, Patch, Param, Delete } from "@nestjs/common";
import { CreateDepartmentDto } from "../dto";
import { DepartmentService } from "../services";

@Controller("department")
export class DepartmentController {
    constructor(private readonly departmentService: DepartmentService) {}

    @Post()
    create(@Body() createDepartmentDto: CreateDepartmentDto) {
        return this.departmentService.create(createDepartmentDto);
    }

    @Get()
    async findAll() {
        const data = await this.departmentService.findAll();
        if (data) {
            return {
                status: 200,
                departments: data,
            };
        }
        return {
            status: 400,
            messageError: "Có lỗi xảy ra",
        };
    }

    @Get(":id")
    async findOne(@Param("id") id: string) {
        const data = await this.departmentService.findOne(id);
        if (data) {
            return {
                status: 200,
                department: data,
            };
        }
        return {
            status: 400,
            messageError: "Có lỗi xảy ra",
        };
    }

    // @Patch(":id")
    // update(@Param("id") id: string, @Body() updateDepartmentDto: UpdateDepartmentDto) {
    //     return this.departmentService.update(+id, updateDepartmentDto);
    // }

    @Delete(":id")
    remove(@Param("id") id: string) {
        return this.departmentService.remove(+id);
    }
}
