import { Body, Controller, Delete, Get, Param, Post, Put, Query, Req, Res, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";

import { BaseUserDto, CreateUserDto } from "../dto/user.dto";
import { UsersService } from "../services";

import { LocalStrategyService } from "./../services/local.stategy.service";
// import { UpdateUserDto } from './dto/update-user.dto';
type ReqToken = {
    token: string;
    refreshToken: string;
};

@Controller("users")
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Post("/register")
    create(@Body() createUserDto: CreateUserDto) {
        return this.usersService.createUser(createUserDto);
    }
    @Post("/login")
    @UseGuards(LocalStrategyService)
    async login(@Res({ passthrough: true }) res, @Body() baseUserDto: BaseUserDto) {
        const response = await this.usersService.loginUser(baseUserDto.username, baseUserDto.password);
        if (response.status == 200) {
            // const refreshToken = await this.usersService.getRefreshToken(baseUserDto.username);
            // const secretData = {
            //     token: response?.access_token,
            //     refreshToken: response?.access_token,
            // };
            // res.cookie("auth-cookie", secretData, { httpOnly: true });
            return {
                status: 200,
                message: "success",
                access_token: response?.access_token,
            };
        } else if (response.status === 400 || response.status === 401) {
            return response;
        }
    }
    @Post("/login/token")
    @UseGuards(LocalStrategyService)
    async loginToken(@Body("token") token: string) {
        const response = await this.usersService.loginUserToken(token);
        if (response.status == 200) {
            return {
                status: 200,
                message: "success",
                access_token: response?.access_token,
            };
        } else if (response.status === 400 || response.status === 401) {
            return response;
        }
    }

    @Get()
    @UseGuards(AuthGuard("jwt"))
    findAll() {
        return this.usersService.findAll();
    }

    @Get("/refresh")
    @UseGuards(AuthGuard("jwt"))
    jwtLoginToken(@Req() req: any) {
        const { token } = req?.cookies["auth-cookie"];
        if (!token) {
            return {
                status: 404,
            };
        }
        return {
            status: 200,
            access_token: token,
        };
    }
    @Get("/department/:id")
    findAllByDepartmentId(@Param("id") id: string) {
        return this.usersService.findAllByDepartmentId(id);
    }
    @Post("/department/:id")
    updateManagerForDepartment(@Param("id") id: string) {
        return this.usersService.updateManagerForDepartment(id);
    }
    @Get(":id")
    findOne(@Param("id") id: string) {
        return this.usersService.findOne(id);
    }

    @Put(":id")
    update(@Param("id") id: string, @Body() user: CreateUserDto) {
        return this.usersService.update(id, user);
    }

    @Delete(":id")
    remove(@Param("id") id: string, @Query("status") status: boolean) {
        return this.usersService.updateStatus(id, status);
    }
}
