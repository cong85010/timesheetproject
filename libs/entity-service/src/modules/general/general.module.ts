import { forwardRef, MiddlewareConsumer, Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";

import { DepartmentController, UsersController, TicketController } from "./controllers";
import { DateEntity, Department, Ticket, User, Week } from "./entities";
import {
    DateService,
    TicketService,
    UsersService,
    WeekService,
    JwtStrategy,
    LocalStrategyService,
    DepartmentService,
} from "./services";

import { CommonModule, env } from "@app/common";
import { PassportModule } from "@nestjs/passport";

const IMPORT = [User, Department, Ticket, DateEntity, Week];
const SERVICES = [UsersService, TicketService, DateService, WeekService, DepartmentService];

@Module({
    imports: [
        TypeOrmModule.forFeature(IMPORT),
        JwtModule.registerAsync({
            useFactory: () => ({
                secret: env.jwt.secretKey,
            }),
        }),
        PassportModule,
        forwardRef(() => CommonModule),
    ],
    providers: [...SERVICES, LocalStrategyService, JwtStrategy],
    exports: [...SERVICES, TypeOrmModule],
    controllers: [UsersController, TicketController, DepartmentController],
})
export class GeneralModule {
    configure(consumer: MiddlewareConsumer) {
        // consumer
        //     .apply(LoggerMiddleware)
        //     .exclude({ path: "login", method: RequestMethod.POST }, { path: "register", method: RequestMethod.POST })
        //     .forRoutes({ path: "*", method: RequestMethod.ALL });
    }
}
