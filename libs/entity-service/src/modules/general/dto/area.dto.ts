import {
    ArrayUnique,
    IsArray,
    IsDateString,
    IsLatitude,
    IsLongitude,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
    IsUrl,
    IsUUID,
    Max,
    Min,
} from "class-validator";
import { Exclude, Expose, Type } from "class-transformer";
import { OmitType } from "@nestjs/swagger";

import { IsCorrectFinishedAt } from "@app/common";

@Exclude()
export class BaseAreaDto {
    @Expose()
    @IsOptional()
    @IsUUID()
    public locationID: string;

    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public ownerID: string;

    @Expose()
    @IsOptional()
    @IsUUID()
    public entityID: string;

    @Expose()
    @IsOptional()
    @IsString()
    public shortID: string;

    @Expose()
    @IsOptional()
    @IsString()
    public code: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    public totalArea: number;

    @Expose()
    @IsOptional()
    @IsNumber()
    public totalQuantity: number;

    @Expose()
    @IsOptional()
    @IsUrl()
    public avatar: string;

    @Expose()
    @IsOptional()
    @IsUrl()
    public avatarThumbnail: string;

    @Expose()
    @IsNotEmpty()
    @IsString()
    public name: string;

    // 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others'
    @IsOptional()
    @Expose()
    @IsNumber()
    public type: number;

    @Expose()
    @IsOptional()
    @IsString()
    public description: string;

    @Expose()
    @IsOptional()
    @IsString()
    public content: string;

    @Expose()
    @IsOptional()
    @IsString()
    public address: string;

    @Expose()
    @IsOptional()
    @IsLatitude()
    public latitude: string;

    @Expose()
    @IsOptional()
    @IsLongitude()
    public longitude: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    public totalProductObject: number;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public updatedAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public deletedAt: string;
}

@Exclude()
export class RawAreaDto {
    @Expose()
    @IsNotEmpty()
    @IsString()
    public name: string;

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    public totalArea: number;

    @Expose()
    @IsNotEmpty()
    @IsString()
    public address: string;

    @Expose()
    @IsNotEmpty()
    @IsString()
    public province: string;

    @Expose()
    @IsNotEmpty()
    @IsString()
    public district: string;

    @Expose()
    @IsOptional()
    @IsString()
    public description: string;

    @Expose()
    @IsArray()
    @IsOptional()
    @IsString()
    public medias: string[];
}

@Exclude()
export class AreaResponseDto extends BaseAreaDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

@Exclude()
class CreateInternalAreaCertificationBodyDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public organizationID: string;

    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public certificationID: string;

    @Expose()
    @IsOptional()
    @IsString()
    public description: string;

    @Expose()
    @IsOptional()
    @IsDateString()
    public issuedAt: string;

    @Expose()
    @IsOptional()
    @IsDateString()
    @IsCorrectFinishedAt("issuedAt")
    public expiredAt: string;

    @Expose()
    @IsOptional()
    @IsArray()
    @ArrayUnique()
    @IsString({ each: true })
    public urls: string[];
}

export class CreateAreaBodyDto extends OmitType(BaseAreaDto, [
    "totalProductObject",
    "code",
    "status",
    "createdAt",
    "updatedAt",
    "deletedAt",
] as const) {}

@Exclude()
export class UpdateAreaBodyDto {
    @Expose()
    @IsOptional()
    @IsUUID()
    public locationID: string;

    @Expose()
    @IsOptional()
    @IsUUID()
    public ownerID: string;

    @Expose()
    @IsOptional()
    @IsUUID()
    public entityID: string;

    @Expose()
    @IsOptional()
    @IsString()
    public shortID: string;

    @Expose()
    @IsOptional()
    @IsArray()
    @ArrayUnique()
    public employeeIDs: string[];

    @Expose()
    @IsOptional()
    @IsString()
    public code: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    public totalArea: number;

    @Expose()
    @IsOptional()
    @IsNumber()
    public totalQuantity: number;

    @Expose()
    @IsOptional()
    @IsUrl()
    public avatar: string;

    @Expose()
    @IsOptional()
    @IsUrl()
    public avatarThumbnail: string;

    @Expose()
    @IsOptional()
    @IsString()
    public name: string;

    // 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others'
    @IsOptional()
    @Expose()
    @IsNumber()
    public type: number;

    @Expose()
    @IsOptional()
    @IsString()
    public description: string;

    @Expose()
    @IsOptional()
    @IsString()
    public content: string;

    @Expose()
    @IsOptional()
    @IsString()
    public address: string;

    @Expose()
    @IsOptional()
    @IsLatitude()
    public latitude: string;

    @Expose()
    @IsOptional()
    @IsLongitude()
    public longitude: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;
}
