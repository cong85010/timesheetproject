import { Expose } from "class-transformer";
import { IsDateString, IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID } from "class-validator";

export class CreateTicketDto {
    @IsString()
    @IsNotEmpty()
    public title: string;

    @Expose()
    @IsOptional()
    @IsString()
    public description: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsOptional()
    @IsUUID()
    public createdBy: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public startTime: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public endTime: string;

    @Expose()
    @IsOptional()
    @IsString()
    public spentTime: string;

    @Expose()
    @IsOptional()
    @IsString()
    public status: string;

    @Expose()
    @IsOptional()
    @IsUUID()
    public userID: string;

    @Expose()
    @IsOptional()
    public date: Date;
}
export class UpdateTicketDto extends CreateTicketDto {
    @Expose()
    @IsOptional()
    @IsUUID()
    public id: string;
}
