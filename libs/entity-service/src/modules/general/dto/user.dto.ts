import { IsEmail, IsNotEmpty, IsOptional, IsString, IsUUID, Length } from "class-validator";

export class BaseUserDto {
    @IsString()
    @IsNotEmpty()
    public username: string;

    @IsNotEmpty()
    public password: string;
}
export class CreateUserDto extends BaseUserDto {
    @IsString()
    @IsNotEmpty()
    public full_name: string;

    @IsEmail()
    public email: string;

    public avatar: string;

    public type: string;

    @IsUUID()
    @IsNotEmpty()
    public department: string;

    @IsOptional()
    public refreshToken: string;

    @IsOptional()
    public refreshTokenExp: string;
}
