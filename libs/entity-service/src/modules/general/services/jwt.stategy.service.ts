import { env } from "@app/common";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { ExtractJwt, Strategy } from "passport-jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt") {
    constructor() {
        super({
            ignoreExpiration: false,
            secretOrKey: env.jwt.secretKey,
            jwtFromRequest: ExtractJwt.fromExtractors([
                (request: Request) => {
                    try {
                        const secretDate = request?.cookies["auth-cookie"];
                        if (!secretDate?.token) {
                            return null;
                        }
                        return secretDate?.token;
                    } catch (error) {
                        return null;
                    }
                },
            ]),
        });
    }

    async validate(payload: any) {
        if (payload === null) {
            throw new UnauthorizedException();
        }
        return payload;
    }
}
