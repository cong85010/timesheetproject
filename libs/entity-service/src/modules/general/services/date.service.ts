import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import * as bcrypt from "bcrypt";
import moment from "moment";
import { Repository } from "typeorm";
import * as uuid from "uuid";
import { CreateTicketDto } from "../dto/ticket.dto";
import { DateEntity, Ticket } from "../entities";
import { WeekService } from "./week.service";

@Injectable()
export class DateService {
    @InjectRepository(DateEntity)
    private readonly dateRepository: Repository<DateEntity>;

    constructor(private jwtService: JwtService, private weekService: WeekService) {}

    async createDateEntity(date: Date): Promise<any> {
        try {
            const dateEntity: DateEntity = new DateEntity();
            dateEntity.id = uuid.v1();
            date = new Date(date);
            dateEntity.date = date.toISOString();
            const week = await this.weekService.findWeekByDate(date);
            if (week.status) {
                dateEntity.week = week.id;
            } else {
                const week = await this.weekService.createWeek(date);
                if (week.status) {
                    dateEntity.week = week.id;
                }
            }
            await this.dateRepository.save(dateEntity);
            return {
                status: 200,
                id: dateEntity.id,
            };
        } catch (error) {
            console.log("====================================");
            console.log(error);
            console.log("====================================");
            return {
                status: 400,
                message: "Có lỗi xảy ra !!! Thử lại sau",
            };
        }
    }
    async findDateOfWeekByDate(date: Date) {
        const week = await this.weekService.findWeekByDate(date);
        if (week.status) {
            return this.dateRepository.find({ where: { week: week.id } });
        }
        return null;
    }
    async findDate(date: Date) {
        return this.dateRepository
            .createQueryBuilder("date")
            .where(`DATE_TRUNC('day', date.date) = '${moment(date).format("YYYY-MM-DD")}';`)
            .getOne();
    }
    update(id: number, accessToken: string, updateTicketDto: CreateTicketDto) {
        return `This action updates a #${id} Ticket`;
    }

    remove(id: number) {
        return `This action removes a #${id} Ticket`;
    }
}
