import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { compareAsc } from "date-fns";
import { Repository } from "typeorm";
import * as uuid from "uuid";
import { CreateTicketDto } from "../dto/ticket.dto";
import { DateEntity, Ticket } from "../entities";
import { UpdateTicketDto } from "./../dto/ticket.dto";
import { DateService } from "./date.service";

@Injectable()
export class TicketService {
    @InjectRepository(Ticket)
    private readonly ticketRepository: Repository<Ticket>;

    constructor(private jwtService: JwtService, private dateService: DateService) {}

    async createTicket(body: CreateTicketDto): Promise<any> {
        try {
            const ticket: Ticket = new Ticket();
            ticket.id = uuid.v1();
            ticket.title = body.title;
            ticket.description = body.description;
            ticket.createdAt = body.createdAt;
            ticket.createdBy = body.createdBy;
            ticket.modifiedAt = body.createdAt;
            ticket.modifiedBy = body.createdBy;
            ticket.startTime = body.startTime;
            ticket.endTime = body.endTime;
            ticket.spentTime = body.spentTime;
            ticket.status = body.status;
            ticket.user = body.userID;
            ticket.userID = body.userID;

            let date = await this.dateService.findDate(body.date);

            if (!date) {
                date = await this.dateService.createDateEntity(body.date);
            }

            ticket.date = date.id;
            ticket.dateId = date.id;
            await this.ticketRepository.save(ticket);
            return {
                status: 200,
                message: "Tạo ticket thành công",
            };
        } catch (error) {
            console.log(error);
            return {
                status: 400,
                message: "Có lỗi xảy ra !!! Thử lại sau",
            };
        }
    }
    async updateTicket(body: UpdateTicketDto): Promise<any> {
        try {
            const ticket: Ticket = new Ticket();
            ticket.id = body.id;
            ticket.title = body.title;
            ticket.description = body.description;
            ticket.createdAt = body.createdAt;
            ticket.createdBy = body.createdBy;
            ticket.modifiedAt = body.createdAt;
            ticket.modifiedBy = body.createdBy;
            ticket.startTime = body.startTime;
            ticket.endTime = body.endTime;
            ticket.spentTime = body.spentTime;
            ticket.status = body.status;
            ticket.user = body.userID;
            ticket.userID = body.userID;
            let date = await this.dateService.findDate(body.date);
            if (!date) {
                date = await this.dateService.createDateEntity(body.date);
            }
            ticket.date = date.id;
            ticket.dateId = date.id;
            await this.ticketRepository
                .createQueryBuilder()
                .update(ticket)
                .where({ id: ticket.id })
                .returning("*")
                .execute();
            return {
                status: 200,
                message: "Cập nhật ticket thành công",
            };
        } catch (error) {
            console.log("====================================");
            console.log(error);
            console.log("====================================");
            return {
                status: 400,
                message: "Có lỗi xảy ra !!! Thử lại sau",
            };
        }
    }
    async findTicketByDateId(userId: string, date: Date) {
        const dateList: DateEntity[] = await this.dateService.findDateOfWeekByDate(date);
        if (dateList) {
            const tickets: Ticket[] = [];
            for (const elem of dateList) {
                const temp: Ticket[] = await this.findTicketByUserIdAndDate(userId, elem.id);
                tickets.push(...temp);
            }
            const result = tickets.sort((a, b) => compareAsc(new Date(a.startTime), new Date(b.startTime)));
            return result;
        }
        return {
            status: 401,
        };
    }

    findTicketByUserId(userId: string) {
        return this.ticketRepository.find({ where: { user: userId } });
    }
    findTicketByUserIdAndDate(userId: string, dateId: string) {
        // return this.ticketRepository.find({ where: [{ user: userId }, { date: dateId }] });
        return this.ticketRepository
            .createQueryBuilder("ticket")
            .where("ticket.user = :userId", { userId: userId })
            .andWhere("ticket.date = :dateId", { dateId: dateId })
            .getMany();
    }
    deleteTicket(id: string) {
        return this.ticketRepository.delete(id);
    }
}
