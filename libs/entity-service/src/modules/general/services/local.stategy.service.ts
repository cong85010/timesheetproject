import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-custom";
import { BaseUserDto } from "../dto";
import { UsersService } from "./users.service";

@Injectable()
export class LocalStrategyService extends PassportStrategy(Strategy) {
    constructor(private userService: UsersService) {
        super({ usernameField: "username" });
    }
    async validate(username: string, password: string): Promise<any> {
        const user = await this.userService.loginUser(username, password);
        console.log(user);

        if (!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
