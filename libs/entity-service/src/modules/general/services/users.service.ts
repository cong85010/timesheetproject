import { DepartmentService } from "./../../../../../../apps/main-api/src/department/department.service";
import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import * as bcrypt from "bcrypt";
import moment from "moment";
import randomToken from "rand-token";
import { Repository } from "typeorm";
import * as uuid from "uuid";

import { CreateUserDto } from "../dto";
import { User } from "../entities";

import { JwtStrategy } from "./jwt.stategy.service";
/* BSCRYP */
const SALT_ROUNDS = 10;

@Injectable()
export class UsersService {
    @InjectRepository(User)
    private readonly userRepository: Repository<User>;

    constructor(private jwtService: JwtService, private jwtStrategy: JwtStrategy) {}
    async getUserByUsernameOrEmail(username: string, email: string): Promise<User | undefined> {
        return this.userRepository
            .createQueryBuilder("user")
            .where("username = :username", { username: username })
            .orWhere("email = :email", { email: email })
            .getOne();
    }

    async createUser(body: CreateUserDto): Promise<any> {
        const response: User = await this.getUserByUsernameOrEmail(body.username, body.email);
        if (response) {
            return {
                status: 400,
                message: "Tài khoản đã được sử dụng",
            };
        }
        try {
            const user: User = new User();
            user.id = uuid.v1();
            user.full_name = body.full_name;
            user.username = body.username;
            user.email = body.email;
            user.password = await bcrypt.hash(body.password, SALT_ROUNDS);
            user.avatar = "";
            user.department = body.department;
            user.departmentId = body.department;
            user.refreshToken = "";
            user.refreshTokenExp = "";
            user.status = true;
            user.type = "EMPLOYEE";

            await this.userRepository.save(user);
            const { id, username, full_name, avatar, status, departmentId, type } = { ...user };
            const token = this.jwtService.sign({ id, username, full_name, status, avatar, departmentId, type });
            user.refreshToken = token;
            await this.userRepository.createQueryBuilder().update(user).where({ id: id }).returning("*").execute();
            return {
                status: 200,
                access_token: token,
            };
        } catch (error) {
            console.log(error);

            return {
                status: 400,
                message: "Có lỗi xảy ra !!! Thử lại sau",
            };
        }
    }
    async loginUser(username: string, password: string) {
        const user: User = await this.getUserByUsernameOrEmail(username, null);
        if (user) {
            const isMatch = await bcrypt.compare(password, user.password);

            if (isMatch) {
                const { id, username, full_name, avatar, status, departmentId, type } = { ...user };
                const token = this.jwtService.sign({ id, username, full_name, status, avatar, departmentId, type });
                user.refreshToken = token;
                await this.userRepository.createQueryBuilder().update(user).where({ id: id }).returning("*").execute();
                return {
                    status: 200,
                    access_token: token,
                };
            }
            return {
                status: 400,
                message: "Có lỗi xảy ra !!! Thử lại sau",
            };
        }

        return {
            status: 401,
            message: "Tài khoản hoặc mật khẩu không tồn tại",
        };
    }
    async loginUserToken(token: string) {
        const user: User = await this.userRepository.findOne({ where: { refreshToken: token } });

        if (user) {
            const { id, username, full_name, avatar, status, departmentId, type, refreshToken } = { ...user };
            const token = this.jwtService.sign({ id, username, full_name, status, avatar, departmentId, type });
            user.refreshToken = token;
            await this.userRepository.createQueryBuilder().update(user).where({ id: id }).returning("*").execute();
            return {
                status: 200,
                access_token: token,
            };
        }

        return {
            status: 401,
            message: "Tài khoản hoặc mật khẩu không tồn tại",
        };
    }
    findAll() {
        return this.userRepository.find();
    }
    async findAllByDepartmentId(id: string) {
        const data: User[] = await this.userRepository.find({ where: { departmentId: id } });
        const newData = data.map(({ id, username, full_name, email, status, avatar, type, departmentId }) => {
            return { id, username, full_name, email, status, avatar, type, departmentId };
        });
        if (newData.length == 0) {
            return {
                status: 400,
                messageError: "Có lỗi xảy ra",
            };
        }
        return {
            status: 200,
            employees: newData,
        };
    }

    findOne(id: string) {
        return this.userRepository.findOne({ where: { id: id } });
    }

    update(id: string, user: CreateUserDto) {
        return this.userRepository.createQueryBuilder().update(user).where({ id: id }).returning("*").execute();
    }

    async updateStatus(id: string, status: boolean) {
        const user: User = await this.findOne(id);
        user.status = !status;
        await this.userRepository
            .createQueryBuilder("user")
            .update(user)
            .where({ id: user.id })
            .returning("*")
            .execute();
        return {
            status: 200,
        };
    }

    async updateManagerForDepartment(id: string) {
        const user: User = await this.findOne(id);
        const departmentId = user.department;
        const manager: User = await this.userRepository.findOne({
            where: [{ department: departmentId, type: "MANAGER" }],
        });
        if (manager) {
            manager.type = "EMPLOYEE";
            await this.userRepository
                .createQueryBuilder()
                .update(manager)
                .where({ id: manager.id })
                .returning("*")
                .execute();
        }
        user.type = "MANAGER";
        return this.userRepository.createQueryBuilder().update(user).where({ id: user.id }).returning("*").execute();
    }

    public async getRefreshToken(username: string): Promise<string> {
        const userDataToUpdate = {
            refreshToken: randomToken.generate(16),
            refreshTokenExp: moment().day(1).format("YYYY/MM/DD"),
        };
        const user = await this.getUserByUsernameOrEmail(username, "");
        // await this.update(user.id, { ...user, ...userDataToUpdate });
        return userDataToUpdate.refreshToken;
    }
}
