import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import moment from "moment";
import { Repository } from "typeorm";
import * as uuid from "uuid";
import { CreateTicketDto } from "../dto/ticket.dto";
import { DateEntity, Week } from "../entities";

@Injectable()
export class WeekService {
    @InjectRepository(Week)
    private readonly weekRepository: Repository<Week>;

    constructor(private jwtService: JwtService) {}

    async createWeek(date: Date): Promise<any> {
        try {
            moment.updateLocale("en", {
                week: {
                    dow: 1, // Monday is the first day of the week.
                },
            });
            const week: Week = new Week();
            week.id = uuid.v1();
            week.name = date.toISOString();
            week.startTime = moment(date).startOf("week").toISOString();
            week.endTime = moment(date).endOf("week").toISOString();
            week.totalTimeSpent = 0;
            this.weekRepository.save(week);
            return {
                status: 200,
                id: week.id,
            };
        } catch (error) {
            return {
                status: 400,
            };
        }
    }

    async findWeekByDate(date: Date): Promise<any> {
        try {
            const week = await this.weekRepository
                .createQueryBuilder("week")
                .where("week.startTime <= :startDate", { startDate: date })
                .andWhere("week.endTime >= :endDate", { endDate: date })
                .getOne();
            return {
                status: true,
                id: week.id,
            };
        } catch (error) {
            return {
                status: false,
            };
        }
    }

    update(id: number, accessToken: string, updateTicketDto: CreateTicketDto) {
        return `This action updates a #${id} Ticket`;
    }

    remove(id: number) {
        return `This action removes a #${id} Ticket`;
    }
}
