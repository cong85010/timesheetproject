import { Repository } from "typeorm";
import { Department } from "./../entities/department.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateDepartmentDto } from "apps/main-api/src/department/dto/create-department.dto";
import { UpdateDepartmentDto } from "apps/main-api/src/department/dto/update-department.dto";

@Injectable()
export class DepartmentService {
    @InjectRepository(Department)
    private readonly departmentRepository: Repository<Department>;
    create(createDepartmentDto: CreateDepartmentDto) {
        return "This action adds a new department";
    }

    findAll() {
        return this.departmentRepository.find();
    }

    findOne(id: string) {
        return this.departmentRepository.findOne({ where: { id: id } });
    }

    update(id: number, updateDepartmentDto: UpdateDepartmentDto) {
        return `This action updates a #${id} department`;
    }

    remove(id: number) {
        return `This action removes a #${id} department`;
    }
}
