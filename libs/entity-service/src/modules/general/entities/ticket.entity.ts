import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { DateEntity } from "./date.entity";
import { User } from "./user.entity";

@Entity()
export class Ticket {
    @PrimaryGeneratedColumn("uuid")
    public id!: string;

    @Column({ type: "varchar", nullable: false })
    public title: string;

    @Column({ type: "varchar", nullable: true  })
    public description: string;

    @Column({ type: "timestamp", nullable: true })
    public createdAt: string;

    @Column({ type: "uuid", nullable: true })
    public createdBy: string;

    @Column({ type: "varchar", nullable: true })
    public modifiedAt: string;

    @Column({ type: "uuid", nullable: true })
    public modifiedBy: string;

    @Column({ type: "timestamp" })
    public startTime: string;

    @Column({ type: "timestamp" })
    public endTime: string;

    @Column({ type: "varchar" })
    public spentTime: string;

    @Column({ type: "varchar" })
    public status: string;
    
    @Column({ name: 'userId' })
    public userID: string;

    @Column({ name: 'dateId' })
    public dateId: string;

    @ManyToOne((type) => User, (user) => user.tickets)
    public user: string;

    @ManyToOne((type) => DateEntity, (date) => date.tickets)
    public date: string;
}
