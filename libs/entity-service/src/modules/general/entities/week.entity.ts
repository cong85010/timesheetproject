import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { DateEntity } from "./date.entity";
import { User } from "./user.entity";

@Entity()
export class Week {
    @PrimaryGeneratedColumn("uuid")
    public id!: string;

    @Column({ type: "varchar" })
    public name: string;

    @Column({ type: "timestamp" })
    public startTime: string;

    @Column({ type: "timestamp" })
    public endTime: string;

    @Column({ type: "int" })
    public totalTimeSpent: number;

    @OneToMany((type) => DateEntity, (date) => date.week)
    public dates: Date[];
}
