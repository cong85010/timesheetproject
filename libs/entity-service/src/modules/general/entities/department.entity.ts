import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Department {
    @PrimaryGeneratedColumn("uuid")
    public id!: string;

    @Column({ type: "varchar" })
    public name: string;

    @OneToMany((type) => User, (user) => user.department)
    public users: User[];
}
