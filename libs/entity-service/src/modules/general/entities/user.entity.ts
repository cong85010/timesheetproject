import { IsOptional, IsUUID } from "class-validator";
import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Department } from "./department.entity";
import { Ticket } from "./ticket.entity";

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    public id: string;

    @Column({ type: "varchar" })
    public full_name: string;

    @Column({ type: "varchar" })
    public email: string;

    @Column({ type: "varchar" })
    public username: string;

    @Column({ type: "varchar" })
    public password: string;

    @Column({ nullable: true })
    public avatar: string;

    @Column()
    public type: string;

    @Column()
    public status: boolean;

    @Column({ nullable: true })
    @IsUUID()
    public departmentId: string;

    @IsUUID()
    @ManyToOne((type) => Department, (department) => department.users)
    public department: string;

    @IsOptional()
    @Column({ nullable: true })
    public refreshToken: string;

    @IsOptional()
    @Column({ nullable: true })
    public refreshTokenExp: string;
    // @OneToMany((type) => Ticket, (ticket) => ticket.userId)
    public tickets: Ticket[];
}
