import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Ticket } from "./ticket.entity";
import { Week } from "./week.entity";

@Entity("date")
export class DateEntity {
    @PrimaryGeneratedColumn("uuid")
    public id!: string;

    @Column({ type: "timestamp" })
    public date: string;

    @ManyToOne((type) => Week, (week) => week.dates)
    public week: string;

    @OneToMany((type) => Ticket, (ticket) => ticket.date)
    public tickets: Ticket[];
}
