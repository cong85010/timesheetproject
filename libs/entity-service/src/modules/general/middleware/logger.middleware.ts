import { Injectable, NestMiddleware, UnauthorizedException } from "@nestjs/common";
import { Request, Response, NextFunction } from "express";

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        const { access_token } = req.headers;

        if (access_token) {
            next();
        } else {
            // res.statusCode = 404;
            // res.setHeader("Content-Type", "text/plain");
            // res.end("Cannot " + req.method + " " + req.url);
            throw new UnauthorizedException();
        }
    }
}
