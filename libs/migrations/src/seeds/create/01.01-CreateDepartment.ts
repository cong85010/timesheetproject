import { Department, User, Ticket, Week, DateEntity } from "@app/entity-service/modules/general/entities";
import { Connection } from "typeorm";
import { Factory, Seeder, times } from "typeorm-seeding";

export class CreateDepartment implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const factoriesDepartment: Department[] = [];
        const factoriesUser: User[] = [];
        const factoriesTicket: Ticket[] = [];
        const factoriesDate: DateEntity[] = [];
        const factoriesWeek: Week[] = [];

        await factory(Week)()
            .map(async (week) => {
                factoriesWeek.push(week);
                return week;
            })
            .createMany(30);

        await factory(Department)()
            .map(async (department) => {
                factoriesDepartment.push(department);
                return department;
            })
            .createMany(5);

        await factory(DateEntity)()
            .map(async (date) => {
                const week: Week = factoriesWeek[Math.floor(Math.random() * factoriesWeek.length)];
                date.week = week.id;
                date.date = week.startTime;
                factoriesDate.push(date);
                date.tickets = [];
                return date;
            })
            .createMany(20);

        await factory(User)()
            .map(async (user) => {
                const deparmentId = factoriesDepartment[Math.floor(Math.random() * factoriesDepartment.length)].id;
                user.department = deparmentId;
                user.departmentId = deparmentId;
                factoriesUser.push(user);
                return user;
            })
            .createMany(30);

        await factory(Ticket)()
            .map(async (ticket) => {
                const uuid = factoriesUser[Math.floor(Math.random() * factoriesUser.length)].id;
                const dateId = factoriesDate[Math.floor(Math.random() * factoriesDate.length)].id;
                ticket.createdBy = uuid;
                ticket.modifiedBy = uuid;
                ticket.user = uuid;
                ticket.userID = uuid;
                ticket.status = "DO";
                ticket.date = dateId
                ticket.dateId = dateId
                return ticket;
            })
            .createMany(50);
    }
}
