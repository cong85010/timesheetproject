// import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

// export class tableUser1655886813028 implements MigrationInterface {
//     public async up(queryRunner: QueryRunner): Promise<void> {
//         const tableDepartment = new Table({
//             name: "department",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false,
//                 },
//                 {
//                     name: "name",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//             ],
//         });
//         const tableUser = new Table({
//             name: "user",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false,
//                 },
//                 {
//                     name: "full_name",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//                 {
//                     name: "email",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//                 {
//                     name: "username",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//                 {
//                     name: "password",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//                 {
//                     name: "avatar",
//                     type: "varchar",
//                     isNullable: true,
//                 },
//                 {
//                     name: "type",
//                     type: "varchar",
//                     isNullable: true,
//                 },
//                 {
//                     name: "departmentId",
//                     type: "uuid",
//                     isNullable: true,
//                 },
//             ],
//         });
//         const tableTicket = new Table({
//             name: "ticket",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false,
//                 },
//                 {
//                     name: "title",
//                     type: "varchar",
//                     isNullable: false,
//                 },
//                 {
//                     name: "description",
//                     type: "varchar",
//                 },
//                 {
//                     name: "createdAt",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "createdBy",
//                     type: "uuid",
//                 },
//                 {
//                     name: "modifiedAt",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "modifiedBy",
//                     type: "uuid",
//                 },
//                 {
//                     name: "startTime",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "endTime",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "spentTime",
//                     type: "varchar",
//                 },
//                 {
//                     name: "status",
//                     type: "varchar",
//                 },
//                 {
//                     name: "userId",
//                     type: "uuid",
//                 },
//                 {
//                     name: "dateId",
//                     type: "uuid",
//                 },
//             ],
//         });
//         const tableDate = new Table({
//             name: "date",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false,
//                 },
//                 {
//                     name: "date",
//                     type: "timestamp",
//                     isNullable: false,
//                 },
//                 {
//                     name: "weekId",
//                     type: "uuid",
//                 },
//             ],
//         });
//         const tableWeek = new Table({
//             name: "week",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false,
//                 },
//                 {
//                     name: "name",
//                     type: "varchar",
//                 },
//                 {
//                     name: "startTime",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "endTime",
//                     type: "timestamp",
//                 },
//                 {
//                     name: "totalTimeSpent",
//                     type: "int",
//                 },
//             ],
//         });
//         await queryRunner.createTable(tableDepartment);
//         await queryRunner.createTable(tableUser);
//         await queryRunner.createTable(tableDate);
//         await queryRunner.createTable(tableWeek);
//         await queryRunner.createTable(tableTicket);
//         await queryRunner.createForeignKey(
//             "user",
//             new TableForeignKey({
//                 name: "fk_department_user",
//                 columnNames: ["departmentId"],
//                 referencedColumnNames: ["id"],
//                 referencedTableName: "department",
//             }),
//         );
//         await queryRunner.createForeignKey(
//             "ticket",
//             new TableForeignKey({
//                 name: "fk_user_ticket",
//                 columnNames: ["userId"],
//                 referencedColumnNames: ["id"],
//                 referencedTableName: "user",
//             }),
//         );

//         await queryRunner.createForeignKey(
//             "ticket",
//             new TableForeignKey({
//                 name: "fk_ticket_date",
//                 columnNames: ["dateId"],
//                 referencedColumnNames: ["id"],
//                 referencedTableName: "date",
//             }),
//         );
//         await queryRunner.createForeignKey(
//             "date",
//             new TableForeignKey({
//                 name: "fk_date_week",
//                 columnNames: ["weekId"],
//                 referencedColumnNames: ["id"],
//                 referencedTableName: "week",
//             }),
//         );
//     }

//     public async down(queryRunner: QueryRunner): Promise<void> {}
// }
