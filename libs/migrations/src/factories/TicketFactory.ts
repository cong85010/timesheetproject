import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { User } from "@app/entity-service/modules/general/entities";
import { CommonUtil } from "@app/common";
import { Ticket } from "@app/entity-service/modules/general/entities/ticket.entity";

define(Ticket, (faker: typeof Faker) => {
    const ticket = new Ticket();

    ticket.id = uuid.v1();
    // ticket.department = CommonUtil.genShortID();
    ticket.title = faker.name.jobTitle();
    ticket.description = faker.lorem.paragraph();
    ticket.createdAt = faker.date.recent().toISOString();
    ticket.modifiedAt = faker.date.recent().toISOString();
    ticket.startTime = faker.date.recent().toISOString();
    ticket.endTime = faker.date.recent().toISOString();
    ticket.spentTime = faker.random.number({ min: 50, max: 100 }) + "";
    return ticket;
});
