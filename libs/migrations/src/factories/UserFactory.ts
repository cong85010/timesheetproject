import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { User } from "@app/entity-service/modules/general/entities";
import { CommonUtil } from "@app/common";

define(User, (faker: typeof Faker) => {
    const user = new User();

    user.id = uuid.v1();
    // user.department = CommonUtil.genShortID();
    user.full_name = faker.name.firstName();
    user.email = faker.internet.exampleEmail();
    user.username = faker.internet.userName();
    user.avatar = faker.internet.avatar();
    user.password = faker.internet.password();
    user.type = "EMPLOYEE"
    user.status = true
    return user;
});
