import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";
import { Week } from "@app/entity-service/modules/general/entities";

define(Week, (faker: typeof Faker) => {
    const week = new Week();
    week.id = uuid.v1();
    week.name = faker.name.jobTitle();
    const d: Date = new Date();
    const plusSevenDay = d.setDate(d.getDate() + 7);
    const sevenDaysFromNow = new Date(plusSevenDay).toISOString();
    week.startTime = d.toISOString() + "";
    week.endTime = sevenDaysFromNow;
    week.totalTimeSpent = faker.random.number({ min: 1, max: 100 });
    return week;
});
