import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { DateEntity } from "@app/entity-service/modules/general/entities";

define(DateEntity, (faker: typeof Faker) => {
    const date = new DateEntity();

    date.id = uuid.v1();

    return date;
});
