import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { Department } from "@app/entity-service/modules/general/entities";
import { CommonUtil } from "@app/common";

define(Department, (faker: typeof Faker) => {
    const department = new Department();

    department.id = uuid.v1();
    department.name = faker.commerce.department()
    
    return department;
});
