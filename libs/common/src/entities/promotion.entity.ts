import { Field, Int, ObjectType } from "@nestjs/graphql";
import { PrimaryColumn } from "typeorm";

@ObjectType()
export class Promotion {
    @PrimaryColumn("uuid")
    @Field()
    public id: string;

    @Field((type) => Int)
    quantity: number;

    @Field((type) => Int)
    discount: number;
}
