import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Theme {
    @Field((type) => String, { nullable: true })
    public color: string;

    @Field((type) => String, { nullable: true })
    public background_color: string;

    @Field((type) => [Frame], { nullable: true })
    public frames?: Frame[];
}

@ObjectType()
export class Frame {
    @Field((type) => String, { nullable: true })
    public id: string;

    // refer to FRAME_TYPE
    @Field((type) => String, { nullable: true })
    public type: string;

    @Field((type) => String, { nullable: true })
    public type_id: string;

    @Field((type) => String, { nullable: true })
    public title: string;

    @Field((type) => Int, { nullable: true })
    public order: number;

    @Field((type) => Boolean, { nullable: true })
    public isHidden: boolean;
}

export const CATALOG_DISPLAY_TYPE = {
    AUTO: 0,
    MANUAL: 1,
};

export const CATALOG_SORT_TYPE = {
    HIGHLIGHT: 0,
    BEST_SELLING: 1,
    LATEST: 2,
    HIGHLY_RECOMMENDED: 3,
    LOWEST_PRICE: 4,
};

export const FRAME_TYPE = {
    AREA: "AREA",
    BANNER: "BANNER",
    CAMPAIGN: "CAMPAIGN",
    CATALOG: "CATALOG",
    CATEGORY: "CATEGORY",
    CERTIFICATE: "CERTIFICATE",
    CO_BUY: "CO_BUY",
    FLASH_SALE: "FLASH_SALE",
    GALLERY: "GALLERY",
    ORIGIN_TRACING: "ORIGIN_TRACING",
    PRODUCT: "PRODUCT",
    STORY: "STORY",
    SUGGESTED_PRODUCT: "SUGGESTED_PRODUCT",
};

const collaboratorData = [
    {
        title: "Banner",
        type: FRAME_TYPE.BANNER,
        order: 0,
        isHidden: false,
    },
    {
        title: "Sản phẩm nổi bật",
        type: FRAME_TYPE.CATALOG,
        catalog: {
            name: "Sản phẩm nổi bật",
            displayType: CATALOG_DISPLAY_TYPE.AUTO,
            sortType: CATALOG_SORT_TYPE.HIGHLIGHT,
        },
        order: 1,
        isHidden: false,
    },
    {
        title: "Nhóm mua chung",
        type: FRAME_TYPE.CO_BUY,
        order: 2,
        isHidden: false,
    },
    {
        title: "Sản phẩm mới",
        type: FRAME_TYPE.PRODUCT,
        order: 3,
        isHidden: false,
    },
];

const storeData = [
    {
        title: "Danh mục",
        type: FRAME_TYPE.CATEGORY,
        order: 0,
        isHidden: false,
    },
    {
        title: "Banner",
        type: FRAME_TYPE.BANNER,
        order: 1,
        isHidden: false,
    },
    {
        title: "Flash Sale",
        type: FRAME_TYPE.FLASH_SALE,
        order: 2,
        isHidden: false,
    },
    {
        title: "Nhóm mua chung",
        type: FRAME_TYPE.CO_BUY,
        order: 3,
        isHidden: false,
    },
    {
        title: "Chiến dịch",
        type: FRAME_TYPE.CAMPAIGN,
        order: 4,
        isHidden: false,
    },
    {
        title: "Sản phẩm mới",
        type: FRAME_TYPE.CATALOG,
        catalog: {
            name: "Sản phẩm mới",
            displayType: CATALOG_DISPLAY_TYPE.AUTO,
            sortType: CATALOG_SORT_TYPE.LATEST,
        },
        order: 5,
        isHidden: false,
    },
    {
        title: "Sản phẩm nổi bật",
        type: FRAME_TYPE.CATALOG,
        catalog: {
            name: "Sản phẩm nổi bật",
            displayType: CATALOG_DISPLAY_TYPE.AUTO,
            sortType: CATALOG_SORT_TYPE.HIGHLIGHT,
        },
        order: 6,
        isHidden: false,
    },
    {
        title: "Sản phẩm bán chạy",
        type: FRAME_TYPE.CATALOG,
        catalog: {
            name: "Sản phẩm bán chạy",
            displayType: CATALOG_DISPLAY_TYPE.AUTO,
            sortType: CATALOG_SORT_TYPE.BEST_SELLING,
        },
        order: 7,
        isHidden: false,
    },
    {
        title: "Gợi ý sản phẩm",
        type: FRAME_TYPE.SUGGESTED_PRODUCT,
        order: 8,
        isHidden: false,
    },
];

const farmData = [
    {
        title: "Sản phẩm",
        type: FRAME_TYPE.PRODUCT,
        order: 0,
        isHidden: false,
    },
    {
        title: "Câu chuyện nông trại",
        type: FRAME_TYPE.STORY,
        order: 1,
        isHidden: false,
    },
    {
        title: "Thư viện",
        type: FRAME_TYPE.GALLERY,
        order: 2,
        isHidden: false,
    },
    {
        title: "Truy xuất nguồn gốc",
        type: FRAME_TYPE.ORIGIN_TRACING,
        order: 3,
        isHidden: false,
    },
    {
        title: "Vùng sản xuất",
        type: FRAME_TYPE.AREA,
        order: 4,
        isHidden: false,
    },
    {
        title: "Chứng nhận",
        type: FRAME_TYPE.CERTIFICATE,
        order: 5,
        isHidden: false,
    },
];

export const frameData = {
    store: storeData,
    farm: farmData,
    collaborator: collaboratorData,
    warehouse: [],
    transport: [],
    volunteer: [],
};
