import { Field, Int, ObjectType } from "@nestjs/graphql";

export enum ADJUSTMENT_TYPE {
    CO_BUY = "CO_BUY",
}
@ObjectType()
export class Adjustment {
    @Field()
    id: string;

    @Field()
    type: string;

    @Field((type) => Int, { nullable: true })
    price?: number;
}
