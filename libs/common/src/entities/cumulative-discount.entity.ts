import { Field, Int, ObjectType } from "@nestjs/graphql";
import { PrimaryColumn } from "typeorm";

@ObjectType()
export class CumulativeDiscount {
    @PrimaryColumn("uuid")
    @Field()
    public id: string;

    @Field((type) => Int)
    public order: number;

    @Field((type) => Int)
    public total_price: number;

    @Field((type) => Int)
    public discount: number;
}
