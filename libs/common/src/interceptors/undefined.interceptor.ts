import { CallHandler, ExecutionContext, NestInterceptor, UseInterceptors } from "@nestjs/common";
import { Observable, map } from "rxjs";
import { ClassConstructor } from "class-transformer";

function UndefinedInterceptor(Clazz: ClassConstructor<any>) {
    return class UndefinedInterceptor implements NestInterceptor {
        intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
            return next.handle().pipe(
                map((value) => {
                    if (!value) throw new Clazz();
                    return value;
                }),
            );
        }
    };
}

// TODO: document this
export const OnUndefinedInterceptor: (clazz: ClassConstructor<any>) => MethodDecorator =
    (clazz: ClassConstructor<any>) => (target, property, descriptor) => {
        UseInterceptors(UndefinedInterceptor(clazz))(target, property, descriptor);
    };
