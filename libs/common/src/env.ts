import * as path from "path";

import * as dotenv from "dotenv";

import * as pkg from "../../../package.json";

import { getOsEnv, getOsEnvOptional, getOsPath, getOsPaths, normalizePort, toBool, toNumber } from "./lib/env";

/**
 * Load .env file or for tests the .env.test file.
 */
dotenv.config({
    path: path.join(process.cwd(), `.env${process.env.NODE_ENV === "test" ? ".test" : ""}`),
});

/**
 * Environment variables
 */
export const env = {
    node: process.env.NODE_ENV || "development",
    isProduction: process.env.NODE_ENV === "production",
    isTest: process.env.NODE_ENV === "test",
    isDevelopment: process.env.NODE_ENV === "development",
    app: {
        name: getOsEnv("APP_NAME"),
        version: (pkg as any).version,
        description: (pkg as any).description,
        host: getOsEnv("APP_HOST"),
        schema: getOsEnv("APP_SCHEMA"),
        routePrefix: getOsEnv("APP_ROUTE_PREFIX"),
        port: normalizePort(process.env.PORT || getOsEnv("APP_PORT")),
        banner: toBool(getOsEnv("APP_BANNER")),
        dirs: {
            migrations: getOsPaths("TYPEORM_MIGRATIONS"),
            migrationsDir: getOsPath("TYPEORM_MIGRATIONS_DIR"),
            entities: getOsPaths("TYPEORM_ENTITIES"),
            entitiesDir: getOsPath("TYPEORM_SEEDING_FACTORIES"),
            controllers: getOsPaths("TYPEORM_SEEDING_SEEDS"),
        },
    },
    hubtech: {
        uploadService: getOsEnv("UPLOAD_SERVICE"),
        pushLogService: getOsEnv("PUSH_LOG_SERVICE"),
        identityServiceV2: getOsEnv("IDENTITY_SERVICE_V2"),
        entityService: getOsEnv("ENTITY_SERVICE"),
        adminWebApp: getOsEnv("HUBADMIN_WEB_APP"),
        webAppHubHub: getOsEnv("HUBHUB_WEB_APP"),
        webAppHubCheck: getOsEnv("HUBCHECK_WEB_APP"),
        webAppHubSupply: getOsEnv("HUBSUPPLY_WEB_APP"),
    },
    entityType: {
        entityTypeUser: toNumber(getOsEnv("ENTITY_TYPE_USER")),
        entityTypeFarm: toNumber(getOsEnv("ENTITY_TYPE_FARM")),
        entityTypeStore: toNumber(getOsEnv("ENTITY_TYPE_STORE")),
        entityTypeCollaborator: toNumber(getOsEnv("ENTITY_TYPE_COLLABORATOR")),
        entityTypeWarehouse: toNumber(getOsEnv("ENTITY_TYPE_WAREHOUSE")),
        entityTypeTransport: toNumber(getOsEnv("ENTITY_TYPE_TRANSPORT")),
        entityTypeVolunteer: toNumber(getOsEnv("ENTITY_TYPE_VOLUNTEER")),
        entityTypeAnonymous: toNumber(getOsEnv("ENTITY_TYPE_ANONYMOUS")),
    },
    defaultValue: {
        defaultAvatar: getOsEnv("DEFAULT_AVATAR"),
        defaultBanner: getOsEnv("DEFAULT_BANNER"),
        defaultProvinceCode: getOsEnv("DEFAULT_PROVINCE_CODE"),
        defaultDistrictCode: getOsEnv("DEFAULT_DISTRICT_CODE"),
        internalAPIkey: getOsEnv("INTERNAL_API_KEY"),
    },
    log: {
        level: getOsEnv("LOG_LEVEL"),
        json: toBool(getOsEnvOptional("LOG_JSON")),
        output: getOsEnv("LOG_OUTPUT"),
        logDriver: getOsEnv("LOG_DRIVER"),
        logDir: getOsEnv("LOG_DIR"),
        logMaxSize: getOsEnv("MAX_SIZE"),
    },
    db: {
        type: getOsEnv("TYPEORM_CONNECTION"),
        host: getOsEnvOptional("TYPEORM_HOST"),
        port: toNumber(getOsEnvOptional("TYPEORM_PORT")),
        username: getOsEnvOptional("TYPEORM_USERNAME"),
        password: getOsEnvOptional("TYPEORM_PASSWORD"),
        database: getOsEnv("TYPEORM_DATABASE"),
        synchronize: toBool(getOsEnvOptional("TYPEORM_SYNCHRONIZE")),
        logging: getOsEnv("TYPEORM_LOGGING"),
        logger: getOsEnv("TYPEORM_LOGGER"),
        defaultPageSize: getOsEnv("TYPEORM_DEFAULT_PAGE_SIZE"),
    },
    graphql: {
        enabled: toBool(getOsEnv("GRAPHQL_ENABLED")),
        route: getOsEnv("GRAPHQL_ROUTE"),
        editor: toBool(getOsEnv("GRAPHQL_EDITOR")),
    },
    swagger: {
        enabled: toBool(getOsEnv("SWAGGER_ENABLED")),
        route: getOsEnv("SWAGGER_ROUTE"),
        username: getOsEnv("SWAGGER_USERNAME"),
        password: getOsEnv("SWAGGER_PASSWORD"),
    },
    monitor: {
        enabled: toBool(getOsEnv("MONITOR_ENABLED")),
        route: getOsEnv("MONITOR_ROUTE"),
        username: getOsEnv("MONITOR_USERNAME"),
        password: getOsEnv("MONITOR_PASSWORD"),
    },
    blockchain: {
        uris: getOsEnv("TENDERMINT_URIS")
            .split(",")
            .map((uri) => uri.trim()),
    },
    redis: {
        caching: getOsEnvOptional("REDIS_CACHING") === "true",
        url: getOsEnvOptional("REDIS_URL"),
        cache_time: +(getOsEnvOptional("REDIS_CACHE_TIME") || 1000),
    },
    momo: {
        domain: {
            production: getOsEnvOptional("MOMO_DOMAIN_PROD") || "https://payment.momo.vn",
            sandbox: getOsEnvOptional("MOMO_DOMAIN_SANDBOX") || "https://test-payment.momo.vn",
        },
        partnerCode: getOsEnv("MOMO_PARTNER_CODE"),
        accessKey: getOsEnv("MOMO_ACCESS_KEY"),
        secretKey: getOsEnv("MOMO_SECRET_KEY"),
    },
    jwt: {
        secretKey: getOsEnv("SECRET_JWT"),
    },
};
