import { IsArray, IsNotEmpty, IsNumber, IsString, IsUUID } from "class-validator";

export class ErrorResponseDto {
    @IsNumber()
    public code: number;

    @IsString()
    public message: string;
}

export class ListDataResponseDto<T> {
    @IsNumber()
    public count: number;

    @IsArray({ each: true })
    public list: T[];
}

export class ListDataDto<T> {
    public count: number;
    public list: T[];
}

export class CurrentEntityDto {
    @IsUUID()
    @IsNotEmpty()
    public id: string;

    @IsNumber()
    @IsNotEmpty()
    public type: number;

    @IsUUID()
    @IsNotEmpty()
    public ownerID: string;

    @IsString()
    @IsNotEmpty()
    public avatar: string;

    @IsString()
    @IsNotEmpty()
    public displayName: string;

    @IsString()
    @IsNotEmpty()
    public fullName: string;

    @IsString()
    @IsNotEmpty()
    public email: string;
}

export class CurrentUserDto {
    @IsUUID()
    @IsNotEmpty()
    public id: string;

    @IsUUID()
    @IsNotEmpty()
    public sub: string;

    @IsString()
    @IsNotEmpty()
    public avatar: string;

    @IsString()
    @IsNotEmpty()
    public displayName: string;

    @IsString()
    @IsNotEmpty()
    public fullName: string;

    @IsString()
    @IsNotEmpty()
    public email: string;
}
