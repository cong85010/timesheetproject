import { Injectable } from "@nestjs/common";
import moment from "moment";
import ShortUniqueId from "short-unique-id";

const MAXIMUM_RANDOM_NUMBER = 100000000;
const MAX_LENGTH_SHORT_ID = 5;

// serialize a BigInt
BigInt.prototype["toJSON"] = function () {
    return this.toString();
};

@Injectable()
export class CommonUtil {
    public static genShortID(): string {
        const uid = new ShortUniqueId({ length: MAX_LENGTH_SHORT_ID });
        uid.setDictionary("alphanum_upper");

        return uid();
    }

    public static checkShortID(id: string): boolean {
        return /^([A-Z0-9]){5}/.test(id);
    }

    public static checkUrl(id: string): boolean {
        const pattern = new RegExp(
            "^(https?:\\/\\/)?" + // protocol
                "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
                "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
                "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
                "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
                "(\\#[-a-z\\d_]*)?$",
            "i",
        ); // fragment locator
        return !!pattern.test(id);
    }

    public subtractTotal(a: number, b: number): number {
        return a > b ? a - b : 0;
    }

    public subtractTotalBigint(a: bigint, b: bigint): bigint {
        return a > b ? a - b : BigInt(0);
    }

    public genOrderCode(): string {
        const uid = new ShortUniqueId({ length: MAX_LENGTH_SHORT_ID });
        uid.setDictionary("alphanum_upper");

        const uniqueNumber = moment().unix();
        const uniqueID = uid();

        return `${uniqueNumber}${uniqueID}`;
    }

    public genShortID(): string {
        const uid = new ShortUniqueId({ length: MAX_LENGTH_SHORT_ID });
        uid.setDictionary("alphanum_upper");

        return uid();
    }

    public checkShortID(id: string): boolean {
        return /^([A-Z0-9]){5}/.test(id);
    }

    public convertToSlug(text: string) {
        const a = "àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;";
        const b = "aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------";
        const p = new RegExp(a.split("").join("|"), "g");
        const formatText = text
            .toString()
            .toLowerCase()
            .replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, "a")
            .replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, "e")
            .replace(/i|í|ì|ỉ|ĩ|ị/gi, "i")
            .replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, "o")
            .replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, "u")
            .replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, "y")
            .replace(/đ/gi, "d")
            .replace(/\s+/g, "-")
            .replace(p, (c) => b.charAt(a.indexOf(c)))
            .replace(/&/g, "-and-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");

        const uniqueNumber = Math.floor(Math.random() * MAXIMUM_RANDOM_NUMBER);
        return `${formatText}.${uniqueNumber}`;
    }

    public static paginate(array: any[], take: number, page: number) {
        return array.slice((page - 1) * take, page * take);
    }

    public static convertToSlug(text: string) {
        const a = "àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;";
        const b = "aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------";
        const p = new RegExp(a.split("").join("|"), "g");
        const formatText = text
            .toString()
            .toLowerCase()
            .replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, "a")
            .replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, "e")
            .replace(/i|í|ì|ỉ|ĩ|ị/gi, "i")
            .replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, "o")
            .replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, "u")
            .replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, "y")
            .replace(/đ/gi, "d")
            .replace(/\s+/g, "-")
            .replace(p, (c) => b.charAt(a.indexOf(c)))
            .replace(/&/g, "-and-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");

        const uniqueNumber = Math.floor(Math.random() * MAXIMUM_RANDOM_NUMBER);
        return `${formatText}.${uniqueNumber}`;
    }

    public static calculatePercentOfANumber(quantity: number, percent: number, fractionDigits = 2) {
        return Number(((quantity * percent) / 100).toFixed(fractionDigits));
    }

    public static removePropertyFromObject(object: any, properties: string[]) {
        if (typeof object === "object" && !Array.isArray(object) && object != null) {
            properties.forEach((property) => {
                delete object[property];
            });
        }
    }

    public static removePropertyFromObjectArray(arr: any[], ...properties: string[]) {
        for (const item of arr) {
            this.removePropertyFromObject(item, properties);
        }
    }
}
