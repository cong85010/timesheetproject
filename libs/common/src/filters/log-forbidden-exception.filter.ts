import { ArgumentsHost, Catch, ExceptionFilter, ForbiddenException } from "@nestjs/common";
import { Response } from "express";

import { env } from "../env";
import { Logger } from "../lib";

@Catch(ForbiddenException)
export class LogForbiddenExceptionFilter implements ExceptionFilter {
    private readonly logger = new Logger(__filename);

    catch(exception: ForbiddenException, host: ArgumentsHost): any {
        const response = host.switchToHttp().getResponse<Response>();
        const status = exception.getStatus();

        this.logger.error(exception.stack);
        response.status(status).json({
            code: status,
            message: exception.message,
            errors: env.isProduction ? undefined : exception.stack,
        });
    }
}
