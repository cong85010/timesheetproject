module.exports = (opts) => {
    return {
        ...opts,
        devtool: "source-map",
    };
};
