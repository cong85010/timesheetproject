# CRON JOB | Scheduler cron job

---

> References:
>
> https://www.npmjs.com/package/cron-decorators
>
> https://www.npmjs.com/package/node-cron
>
> https://en.wikipedia.org/wiki/Cron

Scheduler cron job là một job chạy nền được trigger theo 1 thời gian cụ thể:

VD: hàng tuần, hàng ngày, hàng giờ, mỗi phút, ...



Project chúng ta cần setup cron job để đồng bộ lại các dữ liệu `Diary` bị failed trong khi đẩy sang Tendermint nodes 



## Setup cron job trong project

Có nhiều cách chạy cron job:

- Chạy riêng ở 1 machine khác
- Chạy riêng ở 1 process khác
- Chạy cùng process với server

Ở project này, ta sẽ cho cron job chạy **cùng process với server**:

- có thể dùng chung logic code của app
- vận hành dễ dàng hơn



Setup:

- Sử dụng thư viện [https://www.npmjs.com/package/cron-decorators](https://www.npmjs.com/package/cron-decorators)

- Setup CronLoader cho app và Container (vì project dùng kiến trúc DI pattern)

  ```typescript
  // file src/loaders/cronJobLoader.ts
  export const cronJobLoader: MicroframeworkLoader = async () => {
      registerController([
          path.resolve(__dirname, '../api/cronjobs/*.{ts,js}'),
      ]);
  };
  ```

  ```typescript
  // src/app.ts
  bootstrapMicroframework({
      loaders: [
          // ...
          cronJobLoader,
          // ...
      ],
  })
  ```

  

- Tạo cron job ở file `src/api/cronjobs/*Controller.ts`

- Mỗi khi server chạy (`src/app.ts` chạy) thì cron job cũng bắt đầu chạy theo vào dựa vào thời gian sẽ trigger run các job đã code.



## Định nghĩa 1 Cron-job

> Xem file: `src/api/cronjobs/DiaryCheckCron.ts`

**Example:**

```typescript
@CronController('diaries') // <-- tag cho job
export class DiaryCheckCron {
    constructor(
        @InjectRepository()
        private readonly diarySyncFailedRepository: DiarySyncFailedRepository,
        private readonly tendermintService: TendermintService,
        @Logger(DiaryCheckCron.name)
        private readonly logger: LoggerInterface
    ) {}

    /**
     * run every 5 minute
     */
    @Cron('echo-nodes', '*/5 * * * *') // <-- trigger for job to run
    public echoNodes(): void {
        this.logger.info('start echo nodes');
        this.tendermintService
            .echo()
            .then(() => this.logger.info('node is fine'))
            .catch((error) => this.logger.warn('node is not fine', error));
    }
}
```

