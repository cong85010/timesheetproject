# CACHING | Redis

---

Caching là một phương pháp dùng để lưu tạm thời một giá trị trên bộ nhớ (memory) 
để có thể truy xuất với tốc độ nhanh hơn, so với việc lưu file hoặc database.

Ta có nhiều cách để caching, gồm lưu vào RAM của server (biến global) hoặc lưu vào RAM của 1 service khác - Redis database

Redis database lưu dữ liệu trên RAM, vì thế truy xuất sẽ nhanh hơn nhiều so với truy xuất từ file hay SQL Database.
Thích hợp cho việc caching

## Setup redis

Redis là một dạng database, giống như MySQL hay Postgres, ứng dụng của chúng ta sẽ connect vào và sử dụng chúng.

Redis chạy độc lập với ứng dụng vào được connect thông qua URL:

```
redis://<user>:<password>@<domain>:<port>/<dbname>
```

và thường mặc định là:

```
redis://<user>:<password>@<domain>:6379/<dbname>
```

Để chạy Redis ở local bằng docker-compose, ta setup:

```yaml
# docker-compose.yml
version: ".."
services:
    # ...
    redis:
        container_name: lotus_redis
        image: redis:6-alpine
        ports:
            - 6369:6379
        command: "redis-server --requirepass '123456789'"
        # connect via redis://:123456789@localhost:6379
    # ...
```

Hoặc ta có thể tải redis về máy vào chạy redis

## Using Redis and Cache-Manager

Trong project `LotusFarm`/`farm-service` - được viết trên ngôn ngữ Typescript và Dependency Injection (DI)
tên ta sẽ setup Redis Client trên Node.js và theo DI Pattern luôn

1. Cài `redis` client cho node:

```bash
npm i redis
# or
yarn add redis
```

2. Cài `cache-manager` và `cache-manager-redis-store` cho node:

```bash
npm i cache-manager cache-manager-redis-store
npm i -D @types/cache-manager-redis-store # typing for TS
```

3. Register CacheManager vào Container ở file `src/loaders/cacheStoreLoader.ts`

```typescript
export const cacheStoreLoader: MicroframeworkLoader = (settings) => {
    if (env.redis.caching) {
        // tạo cacheManager để quản lý cache
        const cacheManager = CacheManager.caching({
            // sử dụng Redis để cache
            store: RedisStore.create({
                url: env.redis.url, // <---------------- đọc từ REDIS_URL trong environment variables
                prefix: 'api_cache_',
            }),
            ttl: env.redis.cache_time, // <-------------- đọc từ REDIS_CACHE_TIME
        });
        Container.set(CACHE_MANAGER, cacheManager);
    }
};
```

và ở `src/app.ts` tải loader này lên

```typescript
// ...
bootstrapMicroframework({
    loaders: [
        // ...
        cacheStoreLoader,
        // ...
    ],
})
// ...
```

4. Sử dụng

```typescript

// somewhere
const cache = Container.get(CACHE_MANAGER);
cache.set("key", "value");

// somewhere else
const cache = Container.get(CACHE_MANAGER);
const value = cache.get("key");
```


## Next

- [Request Caching](/caching/request-caching.md)
- [Database Caching](/caching/database-caching.md)
