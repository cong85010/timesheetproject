# GRAPHQL | Setup

----

Các thư viện sử dụng để xây dựng Graphql:

- [type-graphql](https://typegraphql.com/) (Sử dụng để auto-generare GraphQL schema từ Typescript source code)
- [express-graphql](https://graphql.org/graphql-js/express-graphql/) (Sử dụng để handle graphql request cho express)
- [data-loader]() và [type-graphql-dataloader]() (Auto query cái relations trong model)



## 1. Setup GraphQL Server vào Express server

> file: [src/loaders/graphqlLoader.ts]()

Server của ta chạy trên nền là Express server bên dưới.



```typescript
// build schema từ source code
const schema = await buildSchema({
    resolvers: env.app.dirs.resolvers as [string, ...string[]],
    // automatically create `schema.gql` file with schema definition in current folder
    emitSchemaFile: path.resolve(__dirname, '../api', 'schema.gql'),
    validate: true,
    authMode: 'error',
    container: Container,
});

// Đăng ký route /graphql/v1 cho Graphql handler
expressApp.use("/graphql/v1", (request: express.Request, response: express.Response) => {
	// handler code ...
);
```



Handler code:

1. Tạo context cho mỗi request handler

   ```typescript
   // Tạo context cho request, mỗi request sẽ có 1 context riêng
   const requestId = uuidv4();
   const container = Container.of(requestId); // get scoped container
   const context = {
       requestId,
       container,
       request,
       response,
       _tgdContext: {
           // danh cho `type-graphql-dataloader`
           requestId,
           typeormGetConnection: getConnection,
       },
   }; // create our context
   container.set('context', context); // place context or other data in container
   ```

   

2. Xử lý request bằng thư viện []()

   ```typescript
   graphqlHTTP({
       schema,
       context,
       graphiql: env.graphql.editor ? {
           headerEditorEnabled: true,
           defaultQuery: exampleQuery,
       } : false,
       customFormatErrorFn: (error) => ({
           code: getErrorCode(error.message),
           message: getErrorMessage(error.message),
           path: error.path,
           extensions: error.extensions,
           locations: error.locations,
       }),
   })(request, response).finally(() => {
       Container.reset(requestId);
   });
   ```

Code đã setup nằm ở `src/loaders/graphqlLoader.ts`



## 2. Chạy loaders lên

Ở file `src/app.ts` ta đăng ký `graphqlLoader` vào theo thứ tự sau:

```typescript
bootstrapMicroframework({
    loaders: [
        // ...
        typeormLoader,
        // ..
        expressLoader,
        // ...
        graphqlLoader, // graphql luôn phải load sau typeorm và express
        // ...
    ],
})
```

# 3. Next

- [Generate schema cho GraphQL](/graphql/models.md)

- [Tạo resolvers cho Graphql](/graphql/resolvers.md)

  