# Tendermint API

---

>  Tendermint RPC Swagger: [https://docs.tendermint.com/master/rpc/#/](https://docs.tendermint.com/master/rpc/#/)

>  Using Tendermint [https://docs.tendermint.com/master/tendermint-core/using-tendermint.html#broadcast-api](https://docs.tendermint.com/master/tendermint-core/using-tendermint.html#broadcast-api)

## Sử dụng Tendermint trong project

Gửi request thông qua HTTP Request thông thường - sử dụng [axios](https://github.com/axios/axios)

Gửi theo cấu trúc của jsonrpc:

```json
{
    "jsonrpc": "2.0",
    "params": {
        // ... params (can be base64 encoded)
    }
}
```



VD: Ta có data cần truyền là `tx`=`{"name": "Ronaldo", "age": 31}`

```json
{
    "jsonrpc": "2.0",
    "params": {
        "tx": "eyJuYW1lIjoiUm9uYWxkbyIsImFnZSI6MzF9" // data sau khi base64
    }
}
```

